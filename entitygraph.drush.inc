<?php

/**
 * Implements hook_drush_command().
 */
function entitygraph_drush_command() {
  $items['entitygraph'] = array(
    'description' => 'Generate a graph of the entities and fields.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'core' => array('7+'),
    'drupal dependencies' => array('field'),
    'examples' => array(
      'drush entitygraph | dot -Gratio=0.7 -Eminlen=2 -T png -o ./test.png' => 'Generate the Entity-Relationship graph for the current site and export it has a PNG image.',
    ),
  );
  return $items;
}

function drush_entitygraph() {
  $graph = array();

  foreach (entity_get_info() as $entity_type => $entity_info) {
    $node_info = array();
    $node_info['title'] = $entity_info['label'];

    if (!empty($entity_info['base table'])) {
      $table_schema = drupal_get_schema($entity_info['base table']);
      $table_schema += array('fields' => array(), 'foreign keys' => array());
      foreach ($table_schema['fields'] as $field_name => $field_info) {
        $node_info['properties'][$field_name] = array(
          'type' => $field_info['type'],
        );
      }

      foreach ($table_schema['foreign keys'] as $foreign_key_name => $foreign_key_info) {
        if (count($foreign_key_info['columns']) != 1) {
          // We cannot process multiple key foreign keys.
          continue;
        }
        $remote_column = reset($foreign_key_info['columns']);
        $local_column = key($foreign_key_info['columns']);

        $foreign_entity_type = _drush_entitygraph_get_entity_by_table($foreign_key_info['table']);
        if ($foreign_entity_type) {
          $node_info['properties'][$local_column]['type'] = $foreign_entity_type;
          $graph['edges']['entity_' . $entity_type]['entity_' . $foreign_entity_type] = array(
            'arrowhead' => 'normal',
            'headlabel' => '',
            'taillabel' => '',
          );

        _drush_entitygraph_relationship($graph, 'entity_' . $entity_type, 'entity_' . $foreign_entity_type, !empty($table_schema['fields'][$local_column]['not null']), 1);

        }
      }

      // Generate the bundles.
      $has_bundles = !empty($entity_info['entity keys']['bundle']) && count($entity_info['bundles']) > 1;
      if ($has_bundles) {
        foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
          $graph['nodes']['cluster_entity_group_' . $entity_type]['entity_' . $entity_type . '__bundle_' . $bundle_name] = array(
            'title' => $bundle_info['label'],
          );
          $graph['edges']['entity_' . $entity_type . '__bundle_' . $bundle_name]['entity_' . $entity_type] = array(
            'arrowhead' => 'empty',
            'headlabel' => '',
            'taillabel' => '',
          );
        }
      }

      // Traverse the instances.
      $fields = array();
      foreach (field_info_instances($entity_type) as $bundle => $instances) {
        foreach ($instances as $field_name => $instance_info) {
          $fields[$field_name][] = $bundle;
        }
      }

      foreach ($fields as $field_name => $bundles) {
        $field_info = field_info_field($field_name);
        $field_property_info = array(
          'label' => $field_name,
          'type' => $field_info['type'],
        );
        if ($field_info['cardinality'] <> 1) {
          $field_property_info['type'] = 'list<' . $field_property_info['type'] . '>';
        }

        // Build the relationships.
        $relationships = array();
        foreach ($field_info['foreign keys'] as $foreign_key_name => $foreign_key_info) {
          if (count($foreign_key_info['columns']) != 1) {
            // We cannot process multiple key foreign keys.
            continue;
          }
          $remote_column = reset($foreign_key_info['columns']);
          $local_column = key($foreign_key_info['columns']);

          $foreign_entity_type = _drush_entitygraph_get_entity_by_table($foreign_key_info['table']);
          if ($foreign_entity_type) {
            $relationships[] = $foreign_entity_type;
          }
        }

        $all_bundles = count($bundles) == count($entity_info['bundles']);

        if ($all_bundles) {
          // If the field is attached to all the bundles, attach it to the
          // main entity.
          $node_info['fields'][$field_name] = $field_property_info;
          $instance_info = field_info_instance($entity_type, $field_name, key($entity_info['bundles']));
          foreach ($relationships as $relationship) {
            _drush_entitygraph_relationship($graph, 'entity_' . $entity_type, 'entity_' . $relationship, $instance_info['required'], $field_info['cardinality']);
          }
        }
        else {
          foreach ($bundles as $bundle_name) {
            $instance_info = field_info_instance($entity_type, $field_name, $bundle_name);
            $graph['nodes']['cluster_entity_group_' . $entity_type]['entity_' . $entity_type . '__bundle_' . $bundle_name]['fields'][$field_name] = $field_property_info;
            foreach ($relationships as $relationship) {
              _drush_entitygraph_relationship($graph, 'entity_' . $entity_type . '__bundle_' . $bundle_name, 'entity_' . $relationship, $instance_info['required'], $field_info['cardinality']);
            }
          }
        }
      }
    }

    if ($has_bundles) {
      $group = &$graph['nodes']['cluster_entity_group_' . $entity_type];
      $group['label'] = $entity_info['label'];
      $group['group'] = TRUE;
      $group['entity_' . $entity_type] = $node_info;
    }
    else {
      $graph['nodes']['entity_' . $entity_type] = $node_info;
    }
  }

  echo _drush_entitygraph_generate($graph);
}

function _drush_entitygraph_get_entity_by_table($table) {
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!empty($entity_info['base table']) && $entity_info['base table'] == $table) {
      return $entity_type;
    }
  }
}


function _drush_entitygraph_relationship(&$graph, $source, $target, $required, $cardinality) {
  $edge_info = array(
    'arrowhead' => 'normal',
  );
  if ($cardinality >= 1) {
    $min_cardinality = $required ? $cardinality : 0;
    $max_cardinality = $cardinality;
  }
  else {
    $min_cardinality = $required ? 1 : 0;
    $max_cardinality = '*';
  }
  $edge_info['taillabel'] = $min_cardinality . '..' . $max_cardinality;

  $edge_info['headlabel'] = '1..*';

  $graph['edges'][$source][$target] = $edge_info;
}

function _drush_entitygraph_generate($graph) {
  // Merge in defaults.
  $graph += array(
    'nodes' => array(),
    'edges' => array(),
  );

  $output = "digraph G {\n";

  $output .= "node [\n";
  $output .= "shape = \"record\"\n";
  $output .= "]\n";

  foreach ($graph['nodes'] as $name => $node_info) {
    if (!empty($node_info['group'])) {
      $output .= _drush_entitygraph_generate_subgraph($name, $node_info);
    }
    else {
      $output .= _drush_entitygraph_generate_node($name, $node_info);
    }
  }

  foreach ($graph['edges'] as $source_node => $edges) {
    foreach ($edges as $target_node => $edge_info) {
      $output .= "edge [\n";
      foreach ($edge_info as $k => $v) {
        $output .= ' "' . check_plain($k) . '" = "'. check_plain($v) . '"' . "\n";
      }
      $output .= "]\n";
      $output .= $source_node . ' -> ' . $target_node . "\n";
    }
  }

  $output .= "\n}\n";
  return $output;
}

function _drush_entitygraph_generate_subgraph($name, $subgraph_info) {
  $label = $subgraph_info['label'];
  unset($subgraph_info['label']);
  unset($subgraph_info['group']);

  $output = "subgraph $name {\n";
  $output .= 'label = "' . check_plain($label) . '"' . "\n";

  foreach ($subgraph_info as $node_name => $node_info) {
    $output .= _drush_entitygraph_generate_node($node_name, $node_info);
  }

  $output .= "}\n";
  return $output;
}

function _drush_entitygraph_generate_node($name, $node_info) {
  // Merge in defaults.
  $node_info += array(
    'title' => $name,
    'properties' => array(),
    'fields' => array(),
    'methods' => array(),
  );

  $label  = $node_info['title'] . '|';

  foreach ($node_info['properties'] as $property_name => $property_info) {
    $property_info += array(
      'type' => '',
    );
    $label .= $property_name . ' : ' . $property_info['type'] . '\l';
  }

  $label .= '|';

  foreach ($node_info['fields'] as $field_name => $field_info) {
    $field_info += array(
      'type' => '',
    );
    $label .= $field_name . ' : ' . $field_info['type'] . '\l';
  }

  return $name . ' [ label = "{' . check_plain($label) . '}" ]' . "\n";
}
